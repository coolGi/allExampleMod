># Forge/Fabric Example mod
> Allows projects to have Forge and Fabric without rewriting lots of their code
> > Also comes with the neat feature of working for 1.16.5, 1.17.1, 1.18.2, 1.19.2, 1.19.4, 1.20.1, 1.20.2

## What is this project
Its an example mod allowing devs to work on many different versions of Minecraft at once

## For mod devs
Read the doc <a href="https://gitlab.com/coolGi/allExampleMod/-/wikis/home" target="_blank">here</a>


### Credit 
As I also work on another mod called Distant Horizons, most of the gradle code here is similar to the code there (cus I didn't want to rewrite it)
