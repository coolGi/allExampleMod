package com.coolgi.eamplemod;

public class CommonMain {
    public static void init() {
        System.out.println("Inited + " + ModInfo.READABLE_NAME);


        // Quick test for Manifold (should only compile one of these)
        #if MC_1_16_5
        System.out.println("Manifold Preprocessor test..., running 1.16.5");
        #elif MC_1_17_1
        System.out.println("Manifold Preprocessor test..., running 1.17.1");
        #elif MC_1_18_2
        System.out.println("Manifold Preprocessor test..., running 1.18.2");
        #elif MC_1_19_2
        System.out.println("Manifold Preprocessor test..., running 1.19.2");
        #elif MC_1_19_4
        System.out.println("Manifold Preprocessor test..., running 1.19.4");
        #elif MC_1_20_1
        System.out.println("Manifold Preprocessor test..., running 1.20.1");
        #elif MC_1_20_2
        System.out.println("Manifold Preprocessor test..., running 1.20.2");
        #endif
    }
}
