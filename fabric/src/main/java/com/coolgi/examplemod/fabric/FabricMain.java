package com.coolgi.examplemod.fabric;

import com.coolgi.eamplemod.CommonMain;
import net.fabricmc.api.ModInitializer;

public class FabricMain implements ModInitializer {
    @Override
    public void onInitialize() {
        CommonMain.init();
    }
}
